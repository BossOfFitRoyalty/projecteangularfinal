//Pas 2 importar la llibreria Express i inicialitzar una variable amb express
const express = require('express');
const app = express();
//Pas3 Importar les llibreries de cors i body-parser.
const cors = require('cors');
const bodyParser=require ('body-parser');
//Pas 4 Indiquem que utilitzi cors per evitar problemes amb el navegador.
app.use(cors())
//Pas 5 Indicar que utilitzarem json i que rebrem peticions amb url.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended:true
}));
//Pas 6 Indiquem que acceptem peticions pel port 3000:
app.listen(3000, function (){
  console.log('Aplicació nodejs aixecada!!!');
});
module.exports =app;
//Pas 7 Establim la connexió amb la BD:
const client = require('mongodb').MongoClient;
//Puerto localhost cambiado
const url = 'mongodb://localhost:27017';
const dbName = 'albums';
//Pas 8 Creem la petició per defecte “/”:
app.get('/',function (req,res) {
  return res.send({error: false, message: 'hello'})
});

//Ex1
//Este ya esta hecho

//Ex2 Crea una petició get API que retorni tots els usuaris. La url és ‘/users’.
const collectionUsuari = 'usuaris_appv02';
app.get('/users',async (req,
                        res)=>
  {
    let client2;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      const users = await db.collection(collectionUsuari).find().sort({'punts':-1}).toArray();
      res.json(users);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
//Ex3
/*
Crea una petició get API que comprovi si l’usuari i la contrasenya
són iguals al que hi ha a la BD. La dada que es passa és el correu.
Es retorna el número de registres coincidents. Mostra una captura que desde
Postman del què retorna dades. La url és ‘/user/:correu/:password’.
 */

app.get('/user/rec',
  async (req,
         res)=>
  {
    let client2;
    let correu = req.query.nom;
    let password = req.query.password;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      // toArray() -> Això és per retornar tots els elements coincidents
      const users = (await db.collection(collectionUsuari).find({
        'nom': correu,
        'password': password
      }).toArray());
      res.json(users);
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err);
      res.status(500).json({error:'Error intern del servidor'});
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);

/*
  Ex4
 Crea una petició post API que faci un registre d’un nou usuari.
 És a dir, que afegeixi un nou registre.
 Mostra una captura que desde Postman del què retorna dades.
 La url serà ‘/user/insert’.
 */
app.post('/user/insert',
  async (req,
         res)=>
  {
    let client2;
    // Les dades es troben el body
    let nom = req.body.nom;
    let pasword = req.body.password;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      let id = (await db.collection(collectionUsuari).find().toArray()).length + 1;
      // Exemple de insert
      const usuaris = await db.collection(collectionUsuari).
      insertOne({'id': id,'nom':nom, 'password':pasword, 'punts':0});
      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
/*
Ex5
Crea una petició post que actualitza el correu o la contrasenya a partir d’un id.
És a dir, que afegeixi un nou registre. Mostra una captura que desde Postman del què
retorna dades. La url serà ‘/user/update.
 */
app.post('/user/update',
  async (req,
         res)=>
  {
    let client2;
    // Les dades es troben el body
    let nom = req.body.nom;
    let punts =req.body.punts;
    try{
      client2= await client.connect(url);
      const db = client2.db(dbName);
      // Exemple de insert
      const usuaris = await db.collection(collectionUsuari).
      updateOne({'nom': nom},{$set :{'punts':punts}});
      res.json('Guardat!!!');
    }catch (err){
      console.error("Error amb l'operació CRUD de l'API", err)
    } finally{
      if(client2){
        client2.close();
      }
    }
  }
);
