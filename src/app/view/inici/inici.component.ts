import { Component } from '@angular/core';
import {LoginServiceService} from "../../shared/services/login-service.service";
import {ConnectBDService} from "../../shared/services/connect-bd.service";

@Component({
  selector: 'app-inici',
  templateUrl: './inici.component.html',
  styleUrl: './inici.component.css'
})
export class IniciComponent {
  constructor(private connectbd: ConnectBDService, public loginService:LoginServiceService) {
  }
}
