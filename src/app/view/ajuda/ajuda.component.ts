import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {LoginServiceService} from "../../shared/services/login-service.service";

@Component({
  selector: 'app-ajuda',
  templateUrl: './ajuda.component.html',
  styleUrl: './ajuda.component.css'
})
export class AjudaComponent {
  constructor(private connectbd: ConnectBDService, public loginService:LoginServiceService) {
  }
}
