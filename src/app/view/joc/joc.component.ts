import {Component, HostListener} from '@angular/core';
import {LoginServiceService} from "../../shared/services/login-service.service";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {UserClass} from "../../shared/classes/user-class";

@Component({
  selector: 'app-joc',
  templateUrl: './joc.component.html',
  styleUrl: './joc.component.css'
})
export class JocComponent {

  /**
   * Constructor de JocComponent
   * @param loginService Servei per gestionar el iniciar sessió
   * @param connectbd Servei per conectar a la base de dades
   * @param fb Es per facilitar la creació del formBuilder del actualizar
   * dades
   */
  constructor(public loginService:LoginServiceService,
              public connectbd: ConnectBDService,
              public fb: FormBuilder) {}
  board: string[][] = [
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
    ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
  ];

  /**
   * Missatge que indica que s'ha actualitzat els punts,
   * serveix més que res, per comprovar dintre dels testos,
   * si funciona correctament.
   * @type {string}
   */
  missatgeUpdate!:string;
  /**
   * Comprova si s'ha guanyat la partida
   * @type boolean
   */
  ganar !:boolean;
  /**
   * Posicion del jugador inicial
   * @type { x: number; y: number }
   * @default { x: 9, y: 1 } posicio per defecte
   */
  eloiPosition:{ x: number; y: number } = { x: 9, y: 1 };
  //Mensajes de aviso
  /***
   * Dono els missatges d'avisos
   * @type string
   */
  warning!:string;
  /**
   * Variable per el temps del joc, serveix per fer certes
   * funcions en certs temps
   * @Type any
   */
  intervalId !:any;
  /**
   * Variable que comprova si es pot començar amb el joc
   * @Type boolean
   */
  empezarjuego !:boolean;
  /**
   * Conta el numero d'angulars caiguts
   * @Type number
   * @default 0
   */
  numeroFallos:number =0;
  /**
   * Conta el numero d'angulars capturats
   * @Type number
   * @default 0
   */
  numeroCapturados:number =0;

  /**
   * Sirve para crear un formulario, ya que en caso de ganar,
   * mandara los puntos logrados del usuario a la base de datos
   * @type {FormGroup}
   */
  form !:FormGroup;
  /**
   * Aquest es el missatge per  indicar que ha guanyat un angular
   * @type {string}
   */
  missatgeMas!:string;

  /**
   * Aquest es el missatge per indicar que ha perdut un angular
   * @type {string}
   */
  missatgeMenys !:string;

  /**
   * Aquest es el numero per regulat la el temps en que apareixen angulars
   * y es moguin aquests
   * @type {number}
   * @default 1
   */
  vel:number=1;

  /**
   * Variable que conte una array d'usuaris, servira per comprovar
   * al usuari en el qual actualitzarli punts en cas que compleixi els requisits
   * necessaris
   * @type {UserClass[]}
   */
  usuari !: UserClass[];

  /**
   * Funció per iniciar el taulell i jugar,
   * aquesta crida a altres funcions com afegir angulars
   * i fer moure els angulars
   * @param {void}
   * @return el funcionament del joc
   * {moveActivities}  {@link JocComponent#moveActivities}
   * {moveActivities}  {@link JocComponent#addNewActivity}
   */
  startGame():void{

    this.vel=1;
    console.log(this.loginService.userLogin);
    this.resetGame();
    if(this.numeroFallos<10) {
      this.intervalId = setInterval(() => {
        this.moveActivities();
      }, 500/this.vel);
      this.intervalId = setInterval(() => {
        this.addNewActivity();
      }, 3000/this.vel);

    }
  }

  /**
   * Funcio per afegir angulars
   * @param {void}
   * @return retorna un nou angular que s'imprimeix en el taulell
   */
  addNewActivity():void {
    if(this.numeroFallos<10 ) {
      let col = Math.floor(Math.random() * 8) + 1;

      this.board[1][col] = 'activity';
      if(this.numeroCapturados%20 == 0 && this.numeroCapturados!=0){
        this.vel =this.vel*1.5;
      }
      console.log("Agument velocitat "+this.vel);
    }else if(this.numeroFallos>=10){

      this.warning="Has perdut 10 angulars reseteja la partida";
      console.log("Soy este");
      this.comprobarUser();
    }
    }

  /**
   * Funcio que fa moure el jugador
   * @param event rep qualsevol event del teclat per poder mouren's
   * tant per la dreta com per la esquerra
   * @return retorna el movimient del jugador
   */
  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent){
  if(this.empezarjuego && this.numeroFallos<10){
    if(!this.ganar){
      let x:number=this.eloiPosition.x;
      let y:number=this.eloiPosition.y;

      let nuevaX:number=x;
      let nuevaY:number=y;
      switch (event.key) {

        case "ArrowLeft":

          nuevaY = y - 1;

          break;
        case "ArrowRight":

          nuevaY = y + 1;
          break;
        default:

          return;
      }
      this.warning="";
      this.missatgeMenys="";
      this.missatgeMas="";
      if(this.board[nuevaX][nuevaY]=="wall"){
        this.warning="Moviment il·legal et topes amb un mur";
      }else{
        this.eloiPosition.x=nuevaX;
        this.eloiPosition.y=nuevaY;
        this.board[nuevaX][nuevaY]='eloi';
        this.board[x][y]="dot-unhit";
      }
    }
  }else if(this.numeroFallos>=10){
    this.warning="Has perdut 10 angulars reseteja la partida";
  }

  }

  /**
   * Aquesta funció el que fa es fer moure els angulars
   * per que vagin caient
   * @return{void} retorna missatges en cas de aconseguir angulars
   * o perdre'ls.
   */
  moveActivities():void {

    // Mover actividades hacia abajo
    if(this.numeroFallos<10 ) {
      for (let row: number = this.board.length - 2; row >= 0; row--) {
        for (let col: number = 0; col < this.board[row].length; col++) {

          if (this.board[row][col] === 'activity') {

            if (this.board[row + 1][col] !== 'wall'
              && this.board[row + 1][col] !== 'eloi') {
              this.board[row + 1][col] = 'activity';
              this.board[row][col] = 'dot-unhit';
            } else if (this.board[row + 1][col] == "eloi") {
              this.board[row][col] = "dot-unit";
              this.numeroCapturados = this.numeroCapturados + 1;
              this.missatgeMas="Has aconsegit 1";
            } else if (this.board[row + 1][col] == "wall") {
              this.board[row][col] = "dot-unit";
              this.numeroFallos = this.numeroFallos + 1;
              this.missatgeMenys="Has perdut 1";

            }
          }
        }
      }
    }else if(this.numeroFallos>=10){
      this.warning="Has perdut 10 angulars reseteja la partida";
    }

  }

  /**
   * Serveix per resetejar el joc
   * @return{void}
   */
  resetGame():void {
    this.missatgeMas="";
    this.missatgeMas="";
    this.numeroFallos=0;
    this.numeroCapturados=0;
    this.warning="";
    this.vel=1;
    // Reinicia el estado del juego
    this.board = [
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'eloi', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
    ];
    this.eloiPosition = { x: 9, y: 1 };
    this.empezarjuego=true;
  }

  /**
   * Serveix per comprovar l'usuari els seus punts
   * @return{void} en cas de que els punts siguin superiors
   * s'enva a la funció actualizarPunts en cas contrari no fa res
   * {ActualitzarPunts}{@link JocComponent#actualizarPunts}
   */
  comprobarUser():void{
    this.connectbd.getAllUsers().subscribe(res => {
      console.log(res);
     for(let i=0; i<res.length;i++){
        this.usuari =res[i].nom;
       //console.log("Hola? "+ user);
       //console.log("Que soy yo para ti "+ this.loginService.userLogin.value)
       if(this.usuari == this.loginService.userLogin.value){
         //console.log("Si existe este nombre y comparre los puntos")
         if(res[i].punts < this.numeroCapturados){
           //console.log("Hemos recolectado "+this.numeroCapturados+
           //" Y teniamos "+res[i].punts+ " por esta razon cambiamos");
           this.actualizarPunts();

         }else{
           //console.log("Hemos recolectado "+this.numeroCapturados+
             //" Y teniamos "+res[i].punts+ " por esta razon no cambiamos");
         this.usuari=[];
         }
       }
     }
    });
  }

  /**
   * Actualitza els punts, va cap al servei de update punts i actualitza
   * @return{void} en cas de ser cert, retorna un missatge de que ha actualitzat
   * {ConnectBDService Update}{@link ConnectBDService#updatePunts}
   */
  actualizarPunts():void{
     this.form =this.fb.group(
      {
        nom:this.loginService.userLogin.value,
        punts:this.numeroCapturados
      }
    )
    this.connectbd.updatePunts(this.form).subscribe(res => {
      console.log(res);
      console.log("He actualizado?");
      this.missatgeUpdate="Actualizado";

    });
  }
}
