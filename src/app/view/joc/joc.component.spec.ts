import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JocComponent } from './joc.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {of} from "rxjs";

describe('JocComponent', () => {
  let component: JocComponent;
  let fixture: ComponentFixture<JocComponent>;
  let connectbd: ConnectBDService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JocComponent],
      imports:[ReactiveFormsModule, HttpClientTestingModule],
      providers: [ConnectBDService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JocComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Funcion actualitzar datos', () => {
    const res = "Actualizado";
    spyOn(connectbd, 'updatePunts').and.returnValue(of(res));
    component.actualizarPunts();
    fixture.detectChanges();
    expect(component.missatgeUpdate).toContain("Actualizado");
  });
});
