import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {of} from "rxjs";

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let connectbd: ConnectBDService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports:[ReactiveFormsModule, HttpClientTestingModule],
      providers: [ConnectBDService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  /*
  it('funcionament Registre', () => {
    spyOn(connectbd, 'getAllUsers').and.returnValue(of([]));
    spyOn(connectbd, 'insertUser').and.returnValue(of([]));
    component.ngOnInit();
    component.registreForm.patchValue({nom: 'daniTest@ies-sabadell.cat', password:"Super32*"});

    component.registre();
    fixture.detectChanges();
    expect(component.message).toEqual("L'usuari daniTest@ies-sabadell.cat s'ha registrat" +
      " de manera correcte !!");
  });*/
});
