import {Directive, Input} from '@angular/core';
import {AbstractControl, ValidationErrors, Validator, ValidatorFn, Validators} from "@angular/forms";

@Directive({
  selector: '[appValidarPasswordRegister]'
})
export class ValidarPasswordRegisterDirective implements Validator{
  /**
   * Rep del del pare la valicació de la contrasenya
   * @type {boolean}
   * @default false
   */
  @Input('appName') passwordStrengh = false;

  /**
   *
   * Per validar si compleix o no amb la funcio passwordValidator
   * @param control
   * @return {ValidationErrors | null}
   * {passValidator}{@link ValidarPasswordRegisterDirective#passwordValidator}
   */
  validate(control: AbstractControl): ValidationErrors | null {
    return this.passwordStrengh ? passwordValidator()(control):null;
  }
  constructor() { }


}

/**
 * Comprova si la contrasenya enviada
 * conté caràcters especials, numeros i majuscules
 * i minuscules
 * @return {(control: AbstractControl) => (ValidationErrors | null)}
 */
export function passwordValidator():ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null => {
    const value = control.value;
    if(!value){
      return null;
    }
    //EL .test() verifica si es una cadena se repite con un patron definido
    const hasUpperCase:boolean = /[A-Z]+/.test(value);
    const hasLowerCase:boolean = /[a-z]+/.test(value);
    const hasNumeric:boolean = /[0-9]+/.test(value);
    const hasSpecialCharacters:boolean=  /[^\w\d]/.test(value) && !/[#@]/.test(value);
    const passwordValid:boolean =hasUpperCase && hasLowerCase
      && hasNumeric && hasSpecialCharacters;
    return !passwordValid ? {passwordStrength:true}:null;
  };
}

