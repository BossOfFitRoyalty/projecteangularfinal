import {Component, OnInit} from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserClass} from "../../shared/classes/user-class";
import {correoValidator} from "./validar-correo-ins.directive";
import {passwordValidator} from "../login/validar-log-pass.directive";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit{
  /**
   * Es el constructor del RegistreComponent
   * @param connectbd Servei per conectar a la base de dades
   */
  constructor(private connectbd: ConnectBDService) {
  }
  registreForm!: FormGroup;
  users: UserClass[] = [];
  message !: string;
  registreCorrecte !:boolean;

  /**
   * Funció que fa iniciar les dades dels requirement
   * de validacions que hi han en el formulari
   * @return {void} retorna las validacions requerides
   */
  ngOnInit(): void {
    this.registreForm = new FormGroup({
      nom: new FormControl('',
        [Validators.required, Validators.minLength(24),
          Validators.required,correoValidator()]),
      password: new FormControl('', [Validators.required,
        Validators.minLength(8), passwordValidator()])
    })
  }

  /**
   * Funcio que serveix per registrar-se en la base de dades
   * @return {void} Dependent de si existeix o no l'usuari
   * et retornarà un missatge dient si s'ha conectat o no
   */
  registre():void {
    this.connectbd.getAllUsers().subscribe(res => {
      let flat:boolean = false;

      res.forEach((user:any) =>{
        //console.log(user.nom);
        if(user.nom==this.registreForm.value.nom){
          flat=true;

        }
      })

      if(flat){
        this.registreCorrecte=false;
        this.message="Aquest usuari ja existeix "+this.registreForm.value.nom;
      }else {
        this.connectbd.insertUser(this.registreForm).subscribe(res=>{
          this.users=res;
          this.registreCorrecte=true;
          this.message = "L'usuari " + this.registreForm.value.nom + " s'ha registrat" +
            " de manera correcte !!";
        })

      }
    })
  }
  getErrorList(errorObject: {}) {
    return Object.keys(errorObject);
  }
}
