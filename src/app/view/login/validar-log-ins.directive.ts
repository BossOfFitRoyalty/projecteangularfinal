import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[appValidarLogIns]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidarLogInsDirective, multi: true
  }]
})
export class ValidarLogInsDirective implements Validator{

  /**
   * Rep del del pare la valicació del correo
   * @type {boolean}
   * @default false
   */
  @Input('appName') correoValidator = false;

  /**
   * Per validar si compleix o no amb la funcio correoValidator
   * @param control
   * @return {ValidationErrors | null}
   * {correoValidator}{@link ValidarLogInsDirective#correoValidator}
   */
  validate(control: AbstractControl): ValidationErrors | null {
    return this.correoValidator ? correoValidator()(control):null;
  }
  constructor() { }

}

/**
 * Comprova si el correo enviat acaba amb ies-sabadell.cat
 * @return {(control: AbstractControl) => (ValidationErrors | null)}
 */
export function correoValidator():ValidatorFn{
  return (control:AbstractControl):ValidationErrors | null => {
    const value = control.value;
    if(!value){
      return null;
    }
    const finshLikeThat:boolean = value.endsWith('@ies-sabadell.cat');
    return !finshLikeThat ? {correoValidator:true}:null;
  };
}

