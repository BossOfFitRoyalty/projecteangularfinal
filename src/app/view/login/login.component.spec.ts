import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {of} from "rxjs";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let connectbd: ConnectBDService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports:[ReactiveFormsModule, HttpClientTestingModule],
      providers: [ConnectBDService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Funciona Login Funcion', () => {
    const res = [{ nom: 'daniTest@ies-sabadell.cat', password:"Super32*"}];
    spyOn(connectbd, 'checkUser').and.returnValue(of(res));
    component.loginForm.patchValue({nom: 'daniTest@ies-sabadell.cat', password:"Super32*"});
    component.login();
    fixture.detectChanges();
    expect(component.message).toEqual("L'usuari daniTest@ies-sabadell.cat s'ha logejat de manera correcte !!");
  });
});
