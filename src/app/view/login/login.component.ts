import {Component, OnInit} from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserClass} from "../../shared/classes/user-class";
import {correoValidator} from "./validar-log-ins.directive";
import {passwordValidator} from "./validar-log-pass.directive";
import {LoginServiceService} from "../../shared/services/login-service.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  /**
   * Constructor de LoginComponent
   * @param connectbd Servei per conectar a la base de dades
   * @param loginService Servei per gestionar operacions d'inici de sessió
   * @param router Servei per redireccionar a un caltre component
   */
  constructor(private connectbd: ConnectBDService,
              private loginService:LoginServiceService,
              private router:Router) {
  }

  /**
   * Es el formgroup per poder fer login
   * @type {FormGroup}
   */
  loginForm!: FormGroup;
  /**
   * Es una array de la clase jugadors per tal d'obtenir el login del usuari
   * @type {UserClass[]}
   * @default [] per defecte es una array buida
   */
  users: UserClass[] = [];

  /**
   * Missatge per dir si el login ha sigut correcta o incorrecta
   * @type {string}
   *
   */
  message !: string;

  /**
   * Aquest boolean serveix per indicar el tipus de estil que s'enviara.
   * Això vol dir que si es true mostrara una etiqueta amb l'estil bootstrap succes
   * fent visual que ha funcionat, en cas de que sigui incorrecta, mostrarà un missatge
   * amb un estil del bootstrap del tipus danger, per mostrar visualment que és
   * incorrecta.
   * @type {boolean}
   */
  loginCorrecto !:boolean;

  /**
   * Funció que fa iniciar les dades dels requirement
   * de validacions que hi han en el formulari
   * @return {void} retorna las validacions requerides
   *
   */
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      nom: new FormControl('',
        [Validators.required, Validators.minLength(24),
          Validators.required,correoValidator()]),
      password: new FormControl('', [Validators.required,
        Validators.minLength(8), passwordValidator()])
    })
  }

  /**
   * Funció que comprova si l'usuari concideix amb un de la base de dades
   * @return {void} si existeix et retorna a la pàgina del joc
   * en cas contrari et dona un missatge de que no existeix
   * {JocComponent} {@link JocComponent}
   */
  login():void {
    this.connectbd.checkUser(this.loginForm).subscribe(res => {
      console.log(res);
      if (res.length == 0) {
        this.users = [];
        this.message = "Login incorrecte. ";
        this.loginCorrecto=false;
      } else {
        this.users = res;
        console.log(this.users[0]);
        this.message = "L'usuari " + this.users[0].nom + " s'ha logejat" +
          " de manera correcte !!";
        this.loginCorrecto=true;
        this.loginService.updateLoginData(this.users[0].nom);
        this.router.navigate(['/Joc']);
      }
    })
  }

  /**
   * En cas d'error t'avisa el que
   * @param errorObject
   * @return {string[]}
   */
  getErrorList(errorObject: {}) {
    return Object.keys(errorObject);
  }
}




