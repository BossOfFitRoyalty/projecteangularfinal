import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasificacioInformeComponent } from './clasificacio-informe.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {of} from "rxjs";

describe('ClasificacioInformeComponent', () => {
  let component: ClasificacioInformeComponent;
  let fixture: ComponentFixture<ClasificacioInformeComponent>;
  let connectbd:ConnectBDService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClasificacioInformeComponent],
      imports:[ReactiveFormsModule, HttpClientTestingModule],
      providers:[ConnectBDService]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ClasificacioInformeComponent);
    component = fixture.componentInstance;
    connectbd = TestBed.inject(ConnectBDService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Comprovar dades obtenir tots',()=>{
    //data :[ ] Solo en sql
    const res = [{ nom: 'daniTest@ies-sabadell.cat', punts: 10 }];
    spyOn(connectbd,'getAllUsers').and.returnValue(of(res));
    component.getUsuaris();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    console.log("Data: "+component.data);
    expect(component.data).toContain(jasmine.objectContaining({nom:'daniTest@ies-sabadell.cat'}));
  })


});
