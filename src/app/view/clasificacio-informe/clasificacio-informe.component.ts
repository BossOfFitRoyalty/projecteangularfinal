import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserClass} from "../../shared/classes/user-class";
import jsPDF from 'jspdf';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs=pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import {LoginServiceService} from "../../shared/services/login-service.service";
@Component({
  selector: 'app-clasificacio-informe',
  templateUrl: './clasificacio-informe.component.html',
  styleUrl: './clasificacio-informe.component.css'
})
export class ClasificacioInformeComponent implements OnInit{
  /**
   * Constructor de ClasificacioInformeComponent
   * @param connectbd Servei per connectar a la base de dades
   * @param loginService Servei per comprovar si ha iniciat sessió
   */
  constructor(private connectbd: ConnectBDService, public loginService:LoginServiceService) {
  }

  /**
   * Dades de tots els usuaris
   * @type {UserClass[]}
   */
  data !:UserClass[];
  /**
   * Control del formulari per obtenir el camp de text de recerca
   * @type {FormGroup}
   */
  userForm !: FormGroup;
  /**
   * Serveix per mostrar un formulari o no en el html
   * @type {boolean}
   */
  flat !:boolean;

  /**
   * Serveix per mostrar un formulari o no en el html
   * @type {boolean}
   */
  flat2 !:boolean;
  /**
   * Serveix per mostrar un formulari o no en el html
   * @type {boolean}
   */
  flat3 !:boolean;

  /**
   * Inicialitza el component
   * @return {void}
   */
  ngOnInit(): void {
    this.userForm = new FormGroup({
      texto: new FormControl('',
        [Validators.required]),
    })
  }

  /**
   * obté la llista d'usuaris de la
   * base de dades
   */
  getUsuaris():void{

    this.connectbd.getAllUsers().subscribe( res =>{
      console.log(res);
      if(res.length ==0 ){

        this.data=[];

      }else{

        this.data=res;
      }
    });
    this.flat=true;
    this.flat2=false;
    this.flat3=false;

  }


  /**
   * Referencia al element HTML que conté la tabla per generar el PDF
   * @type {ElementRef}
   */
  @ViewChild('pdfTable') pdfTable!: ElementRef;

  /**
   * Genera un PDF de la classificació de jugadors en una nova finestre
   * @return{void}
   */
  obtenirPDF():void{
    this.flat3=true;
    this.flat=false;
    this.flat2=false;
    setTimeout(() => {
      const doc = new jsPDF();
      const pdfTable = this.pdfTable.nativeElement;
      pdfTable.style.display='table';
      var html = htmlToPdfmake(pdfTable.innerHTML);
      const documentDefinition = {content:html};

      pdfMake.createPdf(documentDefinition).open();
    }, 1300);


  }
}
