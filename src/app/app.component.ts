import {Component, OnInit} from '@angular/core';
import {LoginServiceService} from "./shared/services/login-service.service";
import * as string_decoder from "string_decoder";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{
  title = 'angularfinal';
  private usuari:any;
  constructor(public loginservice:LoginServiceService ) {
    //En todos lados donde lo quiera utilizar debere importarlo
    this.loginservice.userLogin.subscribe((data:string)=>this.usuari =data);
  }
  ngOnInit(){
    this.usuari=this.loginservice.userLogin.getValue();
  }
  logOut():void{
    this.loginservice.updateLoginData("logout");
  }
}
