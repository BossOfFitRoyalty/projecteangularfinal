import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {FormGroup} from "@angular/forms";
@Injectable({
  providedIn: 'root'
})
export class ConnectBDService {
  /**
   * Té la URL del servidor API
   * @type string
   * @default hhtp://localhost:3000
   */
  REST_API: string = 'http://localhost:3000';

  /**
   *
   * @param httpclient
   */
  constructor(private httpclient : HttpClient) { }


  //Ex3 Crea un servei d’angular que faci crida a obtenir
  //tots els usuaris, login i register.


  /**
   * Mostrar classificacio
   * @returns {Observable<Object>}
   */
  public getAllUsers():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/users`);
  }

  /**
   * Login
   * @param form
   * @returns {Observable<Object>}
   */
  public checkUser(form: FormGroup):Observable<any>{
    return this.httpclient.get(`${this.REST_API}/user/rec`,
      {params:{nom:form.controls['nom'].value,
          password:form.controls['password'].value}});
  }

  /**
   * Registre
   * @param form
   * @returns {Observable<Object>}
   */
  public insertUser(form: FormGroup):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/user/insert`,
      form.value);
  }

  /**
   * Actualitzar
   * @param form
   * @returns {Observable<Object>}
   */
  public updatePunts(form: FormGroup):Observable<any>{
    console.log("Estoy en esta actualizacion, almenos llego?")
    return this.httpclient.post(`${this.REST_API}/user/update`,
      form.value);
  }

}
