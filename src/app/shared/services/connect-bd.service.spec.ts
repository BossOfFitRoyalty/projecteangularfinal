import { TestBed } from '@angular/core/testing';

import { ConnectBDService } from './connect-bd.service';
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {correoValidator} from "../../view/login/validar-log-ins.directive";
import {passwordValidator} from "../../view/login/validar-log-pass.directive";

describe('ConnectBDService', () => {
  let service: ConnectBDService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let formBuilder:FormBuilder;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post']);
    service = new ConnectBDService(httpClientSpy);
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],providers: [ ConnectBDService ]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ConnectBDService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //obtenir ususaris
  it('Prova GET usuaris obtenints ',()=>{
    service.getAllUsers().subscribe(
      {
        next: users=>{},
        error:fail
      }
    )
    const req = httpTestingController.expectOne(service.REST_API+"/users");
    expect(req.request.method).toEqual('GET');
  })
  //Login
  it("Prova Get login Usuari",()=>{
    let a: FormGroup = new FormGroup({
      nom : new FormControl('daniTest@ies-sabadell.cat'),
      password : new FormControl('Super32Super32*')
    });

    service.checkUser(a).subscribe({
      next: users => {
      },
      error: fail
    });
    const req = httpTestingController.expectOne(service.REST_API+"/user/rec?nom=daniTest@ies-sabadell.cat&password=Super32Super32*");
    expect(req.request.method).toEqual('GET');
  })

  //Insert

  it("Registre proba",()=>{
    let a: FormGroup = new FormGroup({
      nom : new FormControl('daniTest@ies-sabadell.cat'),
      password : new FormControl('Super32Super32*')
    });

    service.insertUser(a).subscribe({
      next: users => {
      },
      error: fail
    });
    const req = httpTestingController.expectOne(service.REST_API + "/user/insert");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.nom).toEqual('daniTest@ies-sabadell.cat');
    expect(req.request.body.password).toEqual('Super32Super32*');
  })

  //UpdatePunts

  it("AActualitza punts",()=>{
    let a: FormGroup = new FormGroup({
      nom : new FormControl('daniTest@ies-sabadell.cat'),
      punts : new FormControl(10)
    });

    service.updatePunts(a).subscribe({
      next: users => {
      },
      error: fail
    });
    const req = httpTestingController.expectOne(service.REST_API + "/user/update");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.nom).toEqual('daniTest@ies-sabadell.cat');
    expect(req.request.body.punts).toEqual(10);
  })




});
