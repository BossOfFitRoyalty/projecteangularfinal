import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor() { }

  userLogin:BehaviorSubject<any> = new BehaviorSubject<any>('logout');
  updateLoginData(user:any){
    this.userLogin.next(user);
  }

}
