import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ConnectBDService} from "../../services/connect-bd.service";
import {ReactiveFormsModule} from "@angular/forms";
import { IniciComponent } from '../../../view/inici/inici.component';
import { LoginComponent } from '../../../view/login/login.component';
import { RegisterComponent } from '../../../view/register/register.component';
import { ClasificacioInformeComponent } from '../../../view/clasificacio-informe/clasificacio-informe.component';
import { AjudaComponent } from '../../../view/ajuda/ajuda.component';
import { ValidarPasswordRegisterDirective } from '../../../view/register/validar-password-register.directive';
import { ValidarCorreoInsDirective } from '../../../view/register/validar-correo-ins.directive';
import { ValidarLogPassDirective } from '../../../view/login/validar-log-pass.directive';
import {ValidarLogInsDirective} from "../../../view/login/validar-log-ins.directive";
import {RouterLink} from "@angular/router";
import {JocComponent} from "../../../view/joc/joc.component";



@NgModule({
  declarations: [
    IniciComponent,
    LoginComponent,
    RegisterComponent,
    ClasificacioInformeComponent,
    AjudaComponent,
    JocComponent,
    ValidarPasswordRegisterDirective,
    ValidarCorreoInsDirective,
    ValidarLogInsDirective,
    ValidarLogPassDirective
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterLink
    ],
  providers:[ConnectBDService],
  exports: [
    IniciComponent,
    LoginComponent,
    RegisterComponent,
    ClasificacioInformeComponent,
    AjudaComponent,
    JocComponent
  ]
})
export class ModulwebModule { }
