import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IniciComponent} from "./view/inici/inici.component";
import {AjudaComponent} from "./view/ajuda/ajuda.component";
import {ClasificacioInformeComponent} from "./view/clasificacio-informe/clasificacio-informe.component";

import {LoginComponent} from "./view/login/login.component";
import {RegisterComponent} from "./view/register/register.component";
import {JocComponent} from "./view/joc/joc.component";

const routes: Routes = [

  {
    path: 'Inici', component:IniciComponent
  },
  {
    path: 'Joc', component:JocComponent
  },
  {
    path: 'Informe', component:ClasificacioInformeComponent
  },
  {
    path: 'Ajuda', component:AjudaComponent
  },
  {
    path: 'Login', component:LoginComponent
  },
  {
    path: 'Registre', component:RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
