'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">angularfinal documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppModule-aea28df5496f317c8eae41c3df5a7e395ab90b809c06e52c4193d22eadcf0cc1b3d7deaf17ac5e07ca6a06fddaa717ee1caf8c7fa4465c189b8224ed21eee324"' : 'data-bs-target="#xs-components-links-module-AppModule-aea28df5496f317c8eae41c3df5a7e395ab90b809c06e52c4193d22eadcf0cc1b3d7deaf17ac5e07ca6a06fddaa717ee1caf8c7fa4465c189b8224ed21eee324"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-aea28df5496f317c8eae41c3df5a7e395ab90b809c06e52c4193d22eadcf0cc1b3d7deaf17ac5e07ca6a06fddaa717ee1caf8c7fa4465c189b8224ed21eee324"' :
                                            'id="xs-components-links-module-AppModule-aea28df5496f317c8eae41c3df5a7e395ab90b809c06e52c4193d22eadcf0cc1b3d7deaf17ac5e07ca6a06fddaa717ee1caf8c7fa4465c189b8224ed21eee324"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModulwebModule.html" data-type="entity-link" >ModulwebModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' : 'data-bs-target="#xs-components-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' :
                                            'id="xs-components-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                            <li class="link">
                                                <a href="components/AjudaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AjudaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ClasificacioInformeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ClasificacioInformeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/IniciComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IniciComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/JocComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JocComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#directives-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' : 'data-bs-target="#xs-directives-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' :
                                        'id="xs-directives-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                        <li class="link">
                                            <a href="directives/ValidarCorreoInsDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidarCorreoInsDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ValidarLogInsDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidarLogInsDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ValidarLogPassDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidarLogPassDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ValidarPasswordRegisterDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidarPasswordRegisterDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' : 'data-bs-target="#xs-injectables-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' :
                                        'id="xs-injectables-links-module-ModulwebModule-7e9d5b7dcca886dd65dc0a68c3dcd28053e5a2b9b0feb320d2633dc6da53fbd4e5447ec18f0ee8787288f9a35231a636e4ba99638af1d13a223bff972e507f97"' }>
                                        <li class="link">
                                            <a href="injectables/ConnectBDService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConnectBDService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/UserClass.html" data-type="entity-link" >UserClass</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ConnectBDService.html" data-type="entity-link" >ConnectBDService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoginServiceService.html" data-type="entity-link" >LoginServiceService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});